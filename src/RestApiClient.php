<?php
namespace Phpid\Adapters;

class RestApiClient
{
    private static $http;

    public function __construct($base_uri)
    {
        static::$http = new HttpClient($base_uri);
        $this->setHeader('Accept', 'application/json');
    }

    public function authenticate($path, $payload)
    {
        $token = $this
            ->post($path, $payload)
            ->response('body');
        $token = json_decode($token, true);
        return $this->authorize($token['accessToken']);
    }

    public function authorize($token, $prefix='Bearer')
    {
        $this->setHeader('Authorization', "{$prefix} $token");
        return $this;
    }

    public function setHeader($header, $value)
    {
        static::$http->header($header, $value);
        return $this;
    }

    public function setAgent($agent)
    {
        $this->setHeader('User-Agent', $agent);
        return $this;
    }

    public function post($path, $payload)
    {
        $payload = $this->complyPayload($payload);
        $this->setHeader('Content-Type', 'application/json');
        return static::$http->request('POST', $path, [], $payload);
    }

    public function get($path)
    {
        return static::$http->request('GET', $path);
    }

    public function put($path, $payload)
    {
        $payload = $this->complyPayload($payload);
        $this->setHeader('Content-Type', 'application/json');
        return static::$http->request('PUT', $path, [], $payload);
    }

    public function delete($path)
    {
        return static::$http->request('DELETE', $path, []);
    }

    private function complyPayload($payload)
    {
        return is_array($payload) ? json_encode($payload) : $payload;
    }
}
