<?php
namespace Phpid\Adapters;

class HttpClient
{
    const METHOD_POST = 'POST';

    const METHOD_GET = 'GET';

    const METHOD_PUT = 'PUT';

    const METHOD_DELETE = 'DELETE';

    private static $instance;

    private static $http;

    protected $base_uri;

    protected $options;

    protected $headers;

    protected $response;

    public function __construct($base_uri, $options=[])
    {
        $this->base_uri = rtrim($base_uri,'\/');

        $this->option(CURLOPT_HEADER, true);
        $this->option(CURLOPT_FOLLOWLOCATION, true);
        $this->option(CURLOPT_RETURNTRANSFER, true);
        $this->option(CURLOPT_TIMEOUT, 90);

        if (!empty($options))
        {
            foreach ($options as $opt=>$val)
            {
                $this->options[$opt] = $val;
            }
        }

        $this->header('User-Agent', 'HttpClient v1.0 https://bitbucket.org/phpid/phplib-http');

        static::$http = curl_init();
    }

    public function __destruct()
    {
        curl_close(static::$http);
    }

    public static function instance()
    {
        return static::$instance;
    }

    public function option($opt, $val)
    {
        $this->options[$opt] = $val;
        return $this;
    }

    public function header($header, $value)
    {
        $this->headers[$header] = $value;
        return $this;
    }

    public function request($method, $path='/', $headers=[], $payload=[])
    {
        $this->switchMethod($method, $payload);
        if (!empty($headers))
        {
            foreach ($headers as $header=>$value)
            {
                $this->header($header, $value);
            }
        }

        $uri = $this->base_uri . $path;
        $this->option(CURLOPT_URL, $uri);

        try
        {
            $this->apply();
            $response = curl_exec(static::$http);
            $header_size = curl_getinfo(static::$http, CURLINFO_HEADER_SIZE);
            $this->response['headers'] = substr($response, 0, $header_size);
            $this->response['http_code'] = curl_getinfo(static::$http, CURLINFO_HTTP_CODE);
            $this->response['body'] = substr($response, $header_size);
            return $this;
        }
        catch (\Exception $e)
        {
            echo $e->getMessage() . PHP_EOL;
            return false;
        }
    }

    /**
     *
     * @param mixed|null $key headers | http_code | body if leave null will return array of response
     * @return mixed
     */

    public function response($key=null)
    {
        return $key ? $this->response[$key] : $this->response;
    }

    private function apply()
    {
        $http_headers = [];
        foreach ($this->headers as $header=>$value)
        {
            $http_headers[] = "{$header}: {$value}";
        }
        $this->option(CURLOPT_HTTPHEADER, $http_headers);

        foreach ($this->options as $opt=>$val)
        {
            curl_setopt(static::$http, $opt, $val);
        }
    }

    private function switchMethod($method, $payload)
    {
        if ($method == static::METHOD_POST)
        {
            $this->option(CURLOPT_POST, true);
            $this->option(CURLOPT_CUSTOMREQUEST, static::METHOD_POST);
            if (!empty($payload))
            {
                $this->option(CURLOPT_POSTFIELDS, $payload);
            }
        }
        elseif ($method == static::METHOD_GET)
        {
            $this->option(CURLOPT_POST, false);
            $this->option(CURLOPT_HTTPGET, true);
            $this->option(CURLOPT_CUSTOMREQUEST, static::METHOD_GET);
            if (isset($this->options['Content-Type']))
            {
                unset($this->options['Content-Type']);
            }
        }
        elseif ($method == static::METHOD_PUT)
        {
            $this->option(CURLOPT_POST, false);
            $this->option(CURLOPT_CUSTOMREQUEST, static::METHOD_PUT);
            $this->option(CURLOPT_POSTFIELDS, $payload);
        }
        elseif ($method == static::METHOD_DELETE)
        {
            $this->option(CURLOPT_POST, false);
            $this->option(CURLOPT_CUSTOMREQUEST, static::METHOD_DELETE);
            if (isset($this->options['Content-Type']))
            {
                unset($this->options['Content-Type']);
            }
        }
    }
}
