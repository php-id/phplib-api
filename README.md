# HTTP and REST Client

> Simple HTTP and REST API Client

## Usage

``` php
use Phpid\Adapters\RestApiClient;

$rest = new RestApiClient('https://postman-echo.com');
$payload = [
    'title' => 'Test Payload REST Client',
    'description' => 'none'
];

// GET Request
echo $rest->get('/get')->response('body') . PHP_EOL;
// POST Request
echo $rest->post('/post', $payload)->response('body') . PHP_EOL;
// PUT Request
echo $rest->put('/put', $payload)->response('body') . PHP_EOL;
// DELETE Request
echo $rest->delete('/delete')->response('body') . PHP_EOL;
```

## Install

Install through `composer`

```
# composer require phpid/lib-api
```
