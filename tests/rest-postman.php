<?php
require __DIR__ . '/../vendor/autoload.php';

use Phpid\Adapters\RestApiClient;

$rest = new RestApiClient('https://postman-echo.com');
$payload = [
    'title' => 'Test Payload REST Client',
    'description' => 'none'
];

echo "GET Request" . PHP_EOL;
echo $rest->get('/get')->response('body') . PHP_EOL;
echo "POST Request" . PHP_EOL;
echo $rest->post('/post', $payload)->response('body') . PHP_EOL;
echo "PUT Request" . PHP_EOL;
echo $rest->put('/put', $payload)->response('body') . PHP_EOL;
echo "DELETE Request" . PHP_EOL;
echo $rest->delete('/delete')->response('body') . PHP_EOL;
